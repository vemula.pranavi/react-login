import React, { useState } from "react";
import { history } from "react-router-dom";
export const Signup = () => {
  const initialInput = {
    firstname: "",
    lastname: "",
    email: "",
    password: ""
  };
  const [input, setinput] = useState(initialInput);
  const handleInput = (e) => {
    setinput({ ...input, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("data submitted");
    const url = "http://localhost:3030/signup";
    fetch(url, { method: "POST", body: input.toString() })
      .then((response) => {
        console.log("response", response.json());
        history.push("/login");
      })
      .catch((error) => {
        console.log("error", error);
      });
  };
  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        name="firstname"
        placeholder="first name"
        onChange={handleInput}
      />
      <input
        type="text"
        name="lastname"
        placeholder="last name"
        onChange={handleInput}
      />
      <input
        type="email"
        name="email"
        placeholder="email"
        onChange={handleInput}
      />
      <input
        type="password"
        name="password"
        placeholder="password"
        onChange={handleInput}
      />
      <button type="submit">sign up</button>
    </form>
  );
};
