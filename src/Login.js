import React, { useState } from "react";
import { history } from "react-router-dom";
export const Login = () => {
  const initialInput = {
    email: "",
    password: ""
  };
  const [input, setinput] = useState(initialInput);
  const handleInput = (e) => {
    setinput({ ...input, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const url = `http://localhost:3030/users/${input.email}/login`;
    const reqBody = { password: input.password };
    fetch(url, { method: "POST", body: reqBody.toString() })
      .then((response) => {
        const res = response.json();
        if (res.status === 200 && res.message && res.message.access_token) {
          localStorage.setItem("isLoggedIn", "true");
          history.push("/home");
        } else localStorage.setItem("isLoggedIn", null);
      })
      .catch((err) => {
        console.log("unexpected error occured");
      });
  };
  return (
    <form id="loginForm" onSubmit={handleSubmit}>
      <input
        type="email"
        name="email"
        placeholder="email"
        onChange={handleInput}
      />
      <input
        type="password"
        name="password"
        placeholder="password"
        onChange={handleInput}
      />
      <button type="submit">login up</button>
    </form>
  );
};
