import "./styles.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Signup } from "./Signup";
import { Login } from "./Login";
import { Home } from "./Home";
export default function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={() => {
              const isLoggedIn = localStorage.getItem("isLoggedIn");
              console.log(isLoggedIn);
              if (isLoggedIn) return <Home />;
              return <Signup />;
            }}
          />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/home" component={Home} />
        </Switch>
      </Router>
      <Login />
    </div>
  );
}
